project(greenfunc LANGUAGES CXX CUDA Fortran)
cmake_minimum_required(VERSION 3.0)

list(APPEND CMAKE_CXX_FLAGS "-fopenmp -std=c++14 -ffast-math")

include_directories(${PROJECT_SOURCE_DIR})

# BOOST
find_package(Boost REQUIRED COMPONENTS filesystem)
if(Boost_FOUND)
  message("-- Boost @ ${Boost_INCLUDE_DIRS}")
  message("-- Boost verison ${Boost_VERSION}")
  include_directories(${Boost_INCLUDE_DIRS})  
endif(Boost_FOUND)

# LAPACK
find_package(LAPACK REQUIRED)
if(LAPACK_FOUND)
  message("-- Lapack lib ${LAPACK_LIBRARIES}")
endif(LAPACK_FOUND)

# CUDA runtime libs
find_package(CUDAToolkit REQUIRED)

# some options to compule libigl viewer
option(LIBIGL_GLFW ON)
option(LIBIGL_IMGUI ON)
option(LIBIGL_OPENGL ON)
option(LIBIGL_WITH_OPENGL "Use OpenGL" ON)
option(LIBIGL_WITH_OPENGL_GLFW "Use GLFW" ON)
option(LIBIGL_WITH_OPENGL_GLFW_IMGUI "Use ImGui" ON)

# HEADER-ONLY LIBS
include_directories(external/eigen/)
include_directories(external/spdlog/include)
include_directories(external/CGAL/include)
include_directories(external/cuda-samples/Common)
add_subdirectory(external/libigl)

add_subdirectory(src)
add_subdirectory(examples)

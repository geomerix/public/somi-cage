#include <igl/min_quad_with_fixed.h>
#include <igl/read_triangle_mesh.h>
#include <igl/opengl/glfw/Viewer.h>
#include <igl/project.h>
#include <igl/unproject.h>
#include <igl/snap_points.h>
#include <igl/unproject_onto_mesh.h>
#include <igl/colon.h>
#include <igl/slice_into.h>
#include <igl/opengl/glfw/imgui/ImGuiPlugin.h>
#include <igl/opengl/glfw/imgui/ImGuiMenu.h>
#include <Eigen/Core>
#include <iostream>
#include <stack>
#include <spdlog/spdlog.h>

#include "src/somigliana_2d.h"
#include "src/vtk.h"

using namespace std;
using namespace Eigen;
using namespace green;

struct State
{
  // Rest and transformed control points
  Eigen::MatrixXd CV, CU;
  bool placing_handles = true;
} s;

namespace {
void ShowHelpMarker(const char* desc)
{
  ImGui::SameLine();
  ImGui::TextDisabled("(?)");
  if (ImGui::IsItemHovered()) {
    ImGui::BeginTooltip();
    ImGui::PushTextWrapPos(450.0f);
    ImGui::TextUnformatted(desc);
    ImGui::PopTextWrapPos();
    ImGui::EndTooltip();
  }
}
}

static void combined_mesh(const somigliana_deformer2 &def,
                          MatrixXi &F, matd_t &V, matd_t &C) {
  // vertices  
  const size_t ncv = def.cageV_.cols(), nv = def.V_.cols();
  V.resize(ncv+nv, 3);
  RowVector3d c = RowVector3d::Zero();
  for (size_t i = 0; i < V.rows(); ++i) {
    if ( i < ncv ) {
      V(i, 0) = def.cageV_(0, i);
      V(i, 1) = 0;
      V(i, 2) = def.cageV_(1, i);
    } else {
      V(i, 0) = def.V_(0, i-ncv);
      V(i, 1) = 0.001;
      V(i, 2) = def.V_(1, i-ncv);
      c.x() += V(i, 0);
      c.z() += V(i, 2);
    }
  }

  // facets
  const size_t ncf = def.cageF_.cols(), nf = def.F_.cols();  
  F.resize(ncf+nf, 3);
  F << def.cageF_.transpose(), (def.F_.transpose().array()+ncv);

  // colors
  C.resize(F.rows(), 3);
  C << RowVector3d(0.2,0.3,0.8).replicate(ncf, 1),
      RowVector3d(1.0,0.7,0.2).replicate(nf, 1);
}

static string deformer_type(DeformerType s) {
  switch ( s ) {
    case SOMIGLIANA: return "somig";
    case GREEN:      return "green";
    case MEANVALUE:  return "mvc";
    case PHONG:      return "phong";
  }
  return "null";
}

static int write_frames(const char *file,
                        const MatrixXd &p,
                        const MatrixXd &q) {
  const MatrixXd P = p.transpose(), Q = q.transpose();
  MatrixXd PQ(P.rows(), P.cols()+Q.cols());
  PQ << P, Q;
  
  const size_t n = PQ.cols()/2;
  MatrixXi line(2, n);
  for (size_t i = 0; i < n; ++i) {
    line(0, i) = i;
    line(1, i) = n+i;
  }

  ofstream ofs(file);
  if ( ofs.fail() ) {
    return __LINE__;
  }
  line2vtk(ofs, PQ.data(), PQ.cols(), line.data(), line.cols());
  ofs.close();
  
  return 0;
}

int main(int argc, char *argv[])
{
  if ( argc != 6 ) {
    cerr << "./igl_coo_deform mesh.off cage.off num_quadrature outdir deformed_cage" << endl;
    return __LINE__;
  }

  const string outdir = argv[4];

  // Undo Management
  std::stack<State> undo_stack,redo_stack;
  const auto push_undo = [&](State & _s=s)
  {
    undo_stack.push(_s);
    // clear
    redo_stack = std::stack<State>();
  };
  const auto undo = [&]()
  {
    if(!undo_stack.empty())
    {
      redo_stack.push(s);
      s = undo_stack.top();
      undo_stack.pop();
    }
  };
  const auto redo = [&]()
  {
    if(!redo_stack.empty())
    {
      undo_stack.push(s);
      s = redo_stack.top();
      redo_stack.pop();
    }
  };

  long sel = -1;
  Eigen::RowVector3f last_mouse;

  // init deformer
  green::somigliana_deformer2 deformer;
  deformer.load_mesh(argv[1]);
  deformer.load_cage(argv[2]);
  deformer.init(atoi(argv[3]));
  spdlog::info("quadrature number={}", atoi(argv[3]));
  {
    char outf[256];
    sprintf(outf, "%s/rest_cage", outdir.c_str());
    deformer.save_cage(outf);
    sprintf(outf, "%s/rest_mesh", outdir.c_str());
    deformer.save_mesh(outf);
  }
  
  // set meshes: cage + original mesh
  Eigen::MatrixXd V, U, D, Ex, Ey;
  Eigen::MatrixXi F;
  Eigen::MatrixXd color;
  combined_mesh(deformer, F, V, color);
  U = V;
  D = Eigen::MatrixXd::Zero(V.rows(), V.cols());
  //  igl::writeOBJ("combined_mesh.obj", V, F);

  // precompute
  deformer.precompute_green_coords();
  deformer.precompute_meanv_coords();
  deformer.precompute_somig_coords();
  spdlog::info("all precomputations done");

  igl::opengl::glfw::Viewer viewer;
  igl::opengl::glfw::imgui::ImGuiPlugin plugin;
  viewer.plugins.push_back(&plugin);
  igl::opengl::glfw::imgui::ImGuiMenu menu;
  plugin.widgets.push_back(&menu);

  // parameters
  DeformerType typeDF = SOMIGLIANA;
  BulgingType typeGamma = SOLID_ANGLE;
  float sc           = 0.0;
  bool plot_frame    = true;
  float poissonR     = 0.0;
  plot_info plt;
  plt.eid_ = 0;
  float sqrt_gamma    = 0.0;
  float blend        = 0.0;
  menu.callback_draw_custom_window = [&]() {
    ImGui::SetNextWindowPos(ImVec2(0.f * menu.menu_scaling(), 600), ImGuiCond_FirstUseEver);
    ImGui::SetNextWindowSize(ImVec2(320, 360), ImGuiCond_FirstUseEver);
    ImGui::Begin("2D Cage deformer", nullptr, ImGuiWindowFlags_NoSavedSettings);
    ImGui::SliderFloat("frame scaling", &sc, 0.0f, 1.0f);
    ImGui::InputFloat("PoissonRatio", &poissonR, 0.00f, 1.0f, "%.3f");
    ImGui::SliderFloat("sqrt_gamma", &sqrt_gamma, 0.0f, 10.0f);
    ImGui::SliderFloat("blend", &blend, 0.0f, 1.0f);

    ImGui::Combo("deformer", reinterpret_cast<int*>(&typeDF),
                 "Somigliana\0Green\0Meanvalue\0\0");
    ImGui::Combo("bulging option", reinterpret_cast<int*>(&typeGamma),
                 "SolidAngle\0SweptVolume\0\0");    

    // plot configuration
    ImGui::InputInt("BoundaryElementID", &plt.eid_);
    ImGui::End();
  };

  // predefined colors
  const Eigen::RowVector3d orange(1.0,0.7,0.2);
  const Eigen::RowVector3d yellow(1.0,0.9,0.2);
  const Eigen::RowVector3d blue(0.2,0.3,0.8);
  const Eigen::RowVector3d green(0.0,0.5,0.5);
  const Eigen::RowVector3d red(0.8,0.2,0.2);
  const Eigen::RowVector3d white(1.0, 1.0, 1.0);
  const Eigen::RowVector3d cyan(0.0, 0.72, 0.92);
  const Eigen::RowVector3d apricot(0.98, 0.81, 0.69);

  const auto & update = [&]()
  {
    if(s.placing_handles) {
      viewer.data().set_vertices(V);
      viewer.data().set_points(s.CV, orange);
      viewer.data().clear_edges();
    } else {
      if ( std::fabs(poissonR-deformer.get_nu())  > 1e-2 ) {        
        // poisson ratio has been changed
        deformer.set_nu(poissonR);
        deformer.precompute_somig_coords();
      }      
      
      Eigen::VectorXi b;
      igl::snap_points(s.CV, V, b);
      
      D.setZero();
      D(b.array(), Eigen::all) = s.CU-s.CV;
      D.col(1).setZero();
      U = V+D;

      // DEFORM!!
      matd_t bc, trac;
      deformer.deform(U, typeDF, typeGamma, sqrt_gamma, blend, plt, Ex, Ey, bc, trac);
        
      viewer.data().set_vertices(U);
      viewer.data().set_points(s.CU, green);
      viewer.data().set_colors(cyan);

      viewer.data().clear_edges();      
      if ( plot_frame ) {
        if ( Ex.size() > 0 && Ey.size() > 0 ) {
          viewer.data().add_edges(U, U+sc*Ex, red);
          viewer.data().add_edges(U, U+sc*Ey, blue);
        }
      } else if ( bc.size() != 0 && trac.size() != 0 ) {
        const int ne = deformer.num_cage_edges(), ncv = deformer.num_cage_vertices();
        viewer.data().add_edges(bc.topRows(ne), bc.topRows(ne)+sc*trac.topRows(ne), red);
        viewer.data().add_edges(bc.bottomRows(ncv), bc.bottomRows(ncv)+sc*trac.bottomRows(ncv), blue);     
      }
    }
  };

  viewer.callback_mouse_down = 
    [&](igl::opengl::glfw::Viewer&, int, int)->bool
  {
    last_mouse = Eigen::RowVector3f(
      viewer.current_mouse_x,viewer.core().viewport(3)-viewer.current_mouse_y,0);
    if(s.placing_handles)
    {
      int fid;
      Eigen::Vector3f bary;
      if(igl::unproject_onto_mesh(
        last_mouse.head(2),
        viewer.core().view,
        viewer.core().proj, 
        viewer.core().viewport, 
        V, F, 
        fid, bary) )
      {
        push_undo();
        s.CV = V.topRows(deformer.num_cage_vertices());
        update();
        return true;        
      }
    } else {
      // Move closest control point
      Eigen::MatrixXf CP;
      igl::project(
        Eigen::MatrixXf(s.CU.cast<float>()),
        viewer.core().view,
        viewer.core().proj, viewer.core().viewport, CP);
      Eigen::VectorXf D = (CP.rowwise()-last_mouse).rowwise().norm();
      sel = (D.minCoeff(&sel) < 30)?sel:-1;
      if(sel != -1)
      {
        last_mouse(2) = CP(sel,2);
        push_undo();
        update();
        return true;
      }
    }
    return false;
  };

  viewer.callback_mouse_up = [&](igl::opengl::glfw::Viewer&, int, int)->bool
  {
    sel = -1;
    return false;
  };
 
  viewer.callback_mouse_move = [&](igl::opengl::glfw::Viewer &, int,int)->bool
  {
    if(sel!=-1)
    {
      Eigen::RowVector3f drag_mouse(
        viewer.current_mouse_x,
        viewer.core().viewport(3) - viewer.current_mouse_y,
        last_mouse(2));
      Eigen::RowVector3f drag_scene,last_scene;
      igl::unproject(
        drag_mouse,
        viewer.core().view,
        viewer.core().proj,
        viewer.core().viewport,
        drag_scene);
      igl::unproject(
        last_mouse,
        viewer.core().view,
        viewer.core().proj,
        viewer.core().viewport,
        last_scene);
      s.CU.row(sel) += (drag_scene-last_scene).cast<double>();
      s.CU(sel, 1) = 0.0;
      last_mouse = drag_mouse;
      update();
      return true;
    }
    return false;
  };

  double trans_flag = 1.0;
  static int count_save_times = 0;
  
  viewer.callback_key_pressed = 
    [&](igl::opengl::glfw::Viewer &, unsigned int key, int mod)
  {
    switch(key)
    { 
      case 'R':
      case 'r':
      {
        push_undo();
        s.CU = s.CV;
        break;
      }
      case 'Q':
      case 'q':
        {
          s.placing_handles = true;
          s.CV.resize(0, 0);
          s.CU.resize(0, 0);
          deformer.clean_angles();
          break;
        }
      case ' ':
        push_undo();
        s.placing_handles ^= 1;
        if(!s.placing_handles && s.CV.rows()>0)
        {
          // Switching to deformation mode
          s.CU = s.CV;
          Eigen::VectorXi b;
          igl::snap_points(s.CV,V,b);
        }
        break;
      case 'j':
      case 'J':
        static int apply_s = 0;
        push_undo();
        if ( !s.placing_handles ) {
          double rate = apply_s%2 == 0 ? 4.0 : 0.25;
          U *= rate;
          s.CU *= rate;
          
          Eigen::VectorXi b;
          igl::snap_points(s.CU, U, b);
          trans_flag *= -1;

          ++apply_s;
        }
        break;        
      case 'g':
      case 'G':
        push_undo();
        static double total_angle = 0.0;
        if ( !s.placing_handles ) {
          const double angle = 10.0/180*M_PI;
          mat2d R;
          R << cos(angle), -sin(angle), sin(angle), cos(angle);
          total_angle += angle;

          // rotate 90 degree
          for (size_t i = 0; i < U.rows(); ++i) {            
            // std::swap(U(i, 0), U(i, 2));
            // U(i, 0) *= -1;
            Vector2d rx = R*Vector2d(U(i, 0), U(i, 2));
            U(i, 0) = rx(0);
            U(i, 2) = rx(1);
          }
          for (size_t i = 0; i < s.CU.rows(); ++i) {
            // std::swap(s.CU(i, 0), s.CU(i, 2));
            // s.CU(i, 0) *= -1;
            Vector2d rx = R*Vector2d(s.CU(i, 0), s.CU(i, 2));
            s.CU(i, 0) = rx(0);
            s.CU(i, 2) = rx(1);
          }

          Eigen::VectorXi b;
          igl::snap_points(s.CU, U, b);
          trans_flag *= -1;
        }
        break;
      case 's':
      case 'S':
        {          
          char outf[256];

          sprintf(outf, "%s/cage-%03d-%s-nu-%f-eps-%f-w-%f", outdir.c_str(), count_save_times, deformer_type(typeDF).c_str(), poissonR, sqrt_gamma*sqrt_gamma, blend);
          deformer.save_cage(outf);

          sprintf(outf, "%s/mesh-%03d-%s-nu-%f-eps-%f-w-%f", outdir.c_str(), count_save_times, deformer_type(typeDF).c_str(), poissonR, sqrt_gamma*sqrt_gamma, blend);
          deformer.save_mesh(outf);

          const int mesh_vert_num = deformer.num_mesh_vertices();
          
          sprintf(outf, "%s/T-nu-%lf-eid-%d-sc-%lf.vtk", outdir.c_str(), poissonR, plt.eid_, sc);
          write_frames(outf, U.bottomRows(mesh_vert_num), (U+sc*Ex).bottomRows(mesh_vert_num));

          sprintf(outf, "%s/N-nu-%lf-eid-%d-sc-%lf.vtk", outdir.c_str(), poissonR, plt.eid_, sc);
          write_frames(outf, U.bottomRows(mesh_vert_num), (U+sc*Ey).bottomRows(mesh_vert_num));
          
          ++count_save_times;
          spdlog::info("save cages {} times", count_save_times);
        }
        break;
      case 'x':
      case 'X':
        {
          if ( !s.placing_handles ) {
            MatrixXi defF; MatrixXd defV;
            igl::read_triangle_mesh(argv[5], defV, defF);
            if ( s.CU.rows() == defV.rows() ) {
              s.CU = defV;
            }
          }
        }
        break;
      default:
        return false;
    }

    update();
    return true;
  };

  viewer.data().set_mesh(V,F);
  viewer.data().show_lines = false;
  viewer.core().is_animating = false;
  viewer.data().face_based = true; 
  viewer.core().background_color.setOnes();
  update();
  return viewer.launch();
}

#include <iostream>
#include <Eigen/Eigen>
#include <unsupported/Eigen/MatrixFunctions>
#include <igl/readOBJ.h>

#define CATCH_CONFIG_MAIN
#include "src/catch.hpp"
#include "src/kelvin_state.h"
#include "src/quadrule.hpp"
#include "src/cuda_impl.h"
#include "src/trig_quad_rule.h"
#include "src/util.h"
#include "src/timer.h"

using namespace std;
using namespace Eigen;

TEST_CASE("eigen", "[add_mat_row_vec]") {
  MatrixXd A = MatrixXd::Random(5, 5).cwiseAbs();
  VectorXd b = VectorXd::Ones(5);
  cout << A << endl << endl;
  A.row(0) += b;
  cout << A << endl << endl;
}

TEST_CASE("kelvin_state", "[differential]") {
  srand(time(NULL));

  const double mu = 1, nu = 0.4;
  green::kelvin_displ kd(mu, nu);

  SECTION("[2D]") {
    const Vector2d r = Vector2d::Random();
    const auto &&K = kd.K(r);
    const auto &&dK = kd.dKdy(r);
    for (size_t i = 0; i < dK.size(); ++i) {
      cout << "analytic " << i << endl << dK[i] << endl << endl;
    }

    const double H = 1e-5;  
    for (size_t i = 0; i < dK.size(); ++i) {
      Vector2d r1 = r, r2 = r;
      r1[i] -= H;
      r2[i] += H;

      const auto &K1 = kd.K(r1), &K2 = kd.K(r2);
      Matrix2d n_dK = (K2-K1)/(2*H);
      cout << "numeric " << i << endl << n_dK << endl << endl;
      REQUIRE((n_dK-dK[i]).norm() < 1e-6*dK[i].norm());
    }
  }

  SECTION("[2D with scaling]") {
    typedef Matrix2d Mat2;

    const Vector2d r = Vector2d::Random();
    const double s = exp(1.0);
    std::vector<Mat2> dK(2, Mat2::Zero());
    kd.dKdy(r, s, dK);
    for (size_t i = 0; i < dK.size(); ++i) {
      cout << "analytic " << i << endl << dK[i] << endl << endl;
    }

    const double H = 1e-5;
    for (size_t i = 0; i < dK.size(); ++i) {
      Vector2d r1 = r, r2 = r;
      r1[i] -= H;
      r2[i] += H;

      const auto &K1 = kd.K(r1), &K2 = kd.K(r2);
      Matrix2d n_dK = s*(K2-K1)/(2*H);
      cout << "numeric " << i << endl << n_dK << endl << endl;
      REQUIRE((n_dK-dK[i]).norm() < 1e-6*dK[i].norm());      
    }
  }

  SECTION("[3D]") {
    const Vector3d r = Vector3d::Random();
    const auto &&K = kd.K(r);
    const auto &&dK = kd.dKdy(r);
    for (size_t i = 0; i < dK.size(); ++i) {
      cout << "analytic " << i << endl << dK[i] << endl << endl;
    }

    const double H = 1e-5;  
    for (size_t i = 0; i < dK.size(); ++i) {
      Vector3d r1 = r, r2 = r;
      r1[i] -= H;
      r2[i] += H;

      const auto &K1 = kd.K(r1), &K2 = kd.K(r2);
      Matrix3d n_dK = (K2-K1)/(2*H);
      cout << "numeric " << i << endl << n_dK << endl << endl;
      REQUIRE((n_dK-dK[i]).norm() < 1e-6*dK[i].norm());
    }    
  }
}

TEST_CASE("kelvin_traction", "[differential]") {
  srand(time(NULL));

  const double mu = 1, nu = 0.4;
  green::kelvin_traction kt(mu, nu);

  SECTION("[2D]") {
    const Vector2d r = Vector2d::Random();
    const Vector2d n = Vector2d::Random().normalized();
    const auto &&T = kt.T(r, n);
    const auto &&dT = kt.dTdy(r, n);
    for (size_t i = 0; i < dT.size(); ++i) {
      cout << "analytic " << i << endl << dT[i] << endl << endl;
    }

    const double H = 1e-5;  
    for (size_t i = 0; i < dT.size(); ++i) {
      Vector2d r1 = r, r2 = r;
      r1[i] -= H;
      r2[i] += H;

      const auto &T1 = kt.T(r1, n), &T2 = kt.T(r2, n);
      Matrix2d n_dT = (T2-T1)/(2*H);
      cout << "numeric " << i << endl << n_dT << endl << endl;
      REQUIRE((n_dT-dT[i]).norm() < 1e-6*dT[i].norm());
    }
  }

  SECTION("[2D with scaling]") {
    const Vector2d r = Vector2d::Random();
    const Vector2d n = Vector2d::Random().normalized();
    const double s = -M_PI;
    std::vector<Matrix2d> dT(2, Matrix2d::Zero());
    kt.dTdy(r, n, s, dT);

    const double H = 1e-5;
    for (size_t i = 0; i < dT.size(); ++i) {
      Vector2d r1 = r, r2 = r;
      r1[i] -= H;
      r2[i] += H;

      const auto &T1 = kt.T(r1, n), &T2 = kt.T(r2, n);
      Matrix2d n_dT = s*(T2-T1)/(2*H);
      REQUIRE((n_dT-dT[i]).norm() < 1e-6*dT[i].norm());      
    }    
  }

  SECTION("[3D]") {
    const Vector3d r = Vector3d::Random();
    const Vector3d n = Vector3d::Random().normalized();
    const auto &&T = kt.T(r, n);
    const auto &&dT = kt.dTdy(r, n);
    for (size_t i = 0; i < dT.size(); ++i) {
      cout << "analytic " << i << endl << dT[i] << endl << endl;
    }

    const double H = 1e-5;  
    for (size_t i = 0; i < dT.size(); ++i) {
      Vector3d r1 = r, r2 = r;
      r1[i] -= H;
      r2[i] += H;

      const auto &T1 = kt.T(r1, n), &T2 = kt.T(r2, n);
      Matrix3d n_dT = (T2-T1)/(2*H);
      cout << "numeric " << i << endl << n_dT << endl << endl;
      REQUIRE((n_dT-dT[i]).norm() < 1e-6*dT[i].norm());
    }
  }
}

TEST_CASE("quadrature", "[1D]") {
  std::vector<double> qp_, qw_;  
  const size_t nt = 64;
  qp_.resize(nt);
  qw_.resize(nt);
  cgqf(nt, 1, 0, 0, 0, 1, -1, &qp_[0], &qw_[0]);
  double sum = 0;
  for (auto &w : qw_) {
    sum += w;
  }
  REQUIRE(fabs(sum-1.0) < 1e-8);
}

TEST_CASE("polor decomposition", "[2D]") {
  srand(time(NULL));

  auto polar2d =
      [](const Eigen::Matrix2d &MM) -> Eigen::Matrix2d {
        Eigen::Matrix2d M = MM;
        double theta = atan2(M(1,0)-M(0,1), M(0,0)+M(1,1));
        M(0, 0) = M(1, 1) = cos(theta);
        M(0, 1) = -sin(theta);
        M(1, 0) = -M(0, 1);        
        return M;
      };

  Eigen::Rotation2D<double> rot(1.0*rand()/RAND_MAX*M_PI);
  Eigen::Matrix2d rotmat = rot.toRotationMatrix();
  cout << "R" << endl << rotmat << endl << endl;
  Eigen::Matrix2d polarM = polar2d(rotmat);
  cout << "S" << endl << polarM.transpose()*rotmat << endl;
  REQUIRE((rotmat-polarM).squaredNorm() < 1e-8);
}

TEST_CASE("mat_func", "[logR]") {
  auto angle_rotmat = 
      [](Eigen::Matrix2d &M) -> double {
        return 180*atan2(M(1,0)-M(0,1), M(0,0)+M(1,1))/M_PI;
      };
    
  cout << "-------------------------------" << endl;
  double a = M_PI-0.01, b = -M_PI+0.01;
  
  Matrix2d R1, L1;
  R1 << cos(a), -sin(a), sin(a), cos(a);
  L1 = R1.log();
  
  Matrix2d R2, L2;
  R2 << cos(b), -sin(b), sin(b), cos(b);
  L2 = R2.log();

  double s = 0.499;
  Matrix2d realL = s*L1+(1-s)*L2;
  Matrix2d realR = realL.exp();
  cout << realL << endl << endl << realR << endl << endl;
  cout << angle_rotmat(realR) << endl;
}

TEST_CASE("triangle quadrature", "[symm]") {
  const size_t nq = 3*8*8;
  Matrix2Xd q; VectorXd w;
  green::sym_trig_quad_rule(nq, q, w);

  auto f =
      [](const double x, const double y)->double {
        return x*x*exp(y)*cos(x*y);
      };

  double sum = 0;
  for (size_t i = 0; i < w.size(); ++i) {
    sum += w(i)*f(q(0, i), q(1, i));
  }
  cout << "numeric integral=" << sum << endl;
}

template <typename T>
static int sgn(T val) {
  return (T(0) < val) - (val < T(0));
}

template <class Mat2>
static Mat2 polar2d (const Mat2 &M) {  
  Mat2 R;
  R(0, 0) = M(1, 1);
  R(0, 1) = -M(1, 0);
  R(1, 0) = -M(0, 1);
  R(1, 1) = M(0, 0);
  R = M+sgn(M.determinant())*R;
  R.col(0).normalize();
  R.col(1).normalize();
  return R;
};

TEST_CASE("polar_decomp_2d", "[performance]") {
  srand(time(NULL));

  green::high_resolution_timer clk;
  double T_A = 0, T_B = 0;  
  for (int i = 0; i < 100; ++i) {
    Matrix2d A = Matrix2d::Random()+0.2*Matrix2d::Identity();

    clk.start();
    Matrix2d R = green::polar2d_a(A);
    clk.stop();    
    T_A += clk.duration();
    Matrix2d S = R.transpose()*A;

    clk.start();
    Matrix2d RR = green::polar2d_b(A);
    clk.stop();
    T_B += clk.duration();
    Matrix2d SS = RR.transpose()*A;

    REQUIRE((R-RR).norm() < 1e-8*R.norm());
    REQUIRE((S-SS).norm() < 1e-8*S.norm());
    REQUIRE((A-R*S).norm() < 1e-8*A.norm());
    REQUIRE((S-S.transpose()).norm() < 1e-8*S.norm());    
  }
  cout << "polar time: " << T_A << " " << T_B << endl;
}

TEST_CASE("polar_decomp_3d", "[performance]") {
  srand(time(NULL));

  green::high_resolution_timer clk;
  double T_A = 0, T_B = 0;  
  for (int i = 0; i < 100; ++i) {
    Vector3d axis = Vector3d::Random().normalized();
    double angle = 1.0*rand()/RAND_MAX*M_PI;

    Matrix3d A, Sym;
    {
      Sym = Matrix3d::Random();
      A = AngleAxisd(angle, axis);
      A *= (Sym.transpose()*Sym+Matrix3d::Identity());
    }

    clk.start();
    Matrix3d R = green::polar3d_a(A, 100);
    clk.stop();    
    T_A += clk.duration();
    Matrix3d S = R.transpose()*A;

    clk.start();
    Matrix3d RR = green::polar3d_b(A);
    clk.stop();
    T_B += clk.duration();
    Matrix3d SS = RR.transpose()*A;

    REQUIRE((R-RR).norm() < 1e-6*R.norm());
    REQUIRE((S-SS).norm() < 1e-6*S.norm());
    REQUIRE((A-R*S).norm() < 1e-6*A.norm());
    REQUIRE((S-S.transpose()).norm() < 1e-6*S.norm());    
  }
  cout << "polar time: " << T_A << " " << T_B << endl;
}

TEST_CASE("rotation between vectors", "[3D]") {
  srand(time(NULL));
  for (int i = 0; i < 100; ++i) {
    Vector3d a = Vector3d::Random().normalized();
    Vector3d b = Vector3d::Random().normalized();
    Matrix3d R = (Quaterniond().setFromTwoVectors(a, b)).toRotationMatrix();
    REQUIRE((R*a-b).norm() < 1e-8);
  }
}

TEST_CASE("quad area", "[2D]") {
  {
    Vector2d a(0, 0), b(1, 0), c(1, 1), d(0, 1);
    cout << "area=" << green::quad_area_2d<Vector2d>(a, b, c, d) << endl;
  }
  {
    Vector2d a(0, 0), b(1, -1), c(2, 0), d(1, 1);
    cout << "area=" << green::quad_area_2d<Vector2d>(a, b, c, d) << endl;
  }
  {
    Vector2d a(0, 0), b(1, -1), c(1, 0), d(1, 1);
    cout << "area=" << green::quad_area_2d<Vector2d>(a, b, c, d) << endl;
  }    
}

TEST_CASE("volume", "[triangles]") {
  MatrixXd V;
  MatrixXi F;
  igl::readOBJ("../data/uv_sphere.obj", V, F);

  double vol = 0;
  for (size_t i = 0; i < F.rows(); ++i) {
    vol += green::tri_sgn_vol<Eigen::RowVector3d>(V.row(F(i, 0)), V.row(F(i, 1)), V.row(F(i, 2)));
  }
  cout << vol << endl;
  cout << 4.0/3*M_PI << endl;
}

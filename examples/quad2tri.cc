#include <iostream>
#include <fstream>
#include <igl/readOBJ.h>
#include <igl/writeOBJ.h>

using namespace std;
using namespace Eigen;

int main(int argc, char *argv[])
{
  if ( argc != 3 ) {
    cerr << "quad2tri input.obj output.obj" << endl;
  }

  MatrixXi quad; MatrixXd nods;
  igl::readOBJ(argv[1], nods, quad);
  cout << "quad: " << quad.rows() << " " << quad.cols() << endl;
  cout << "nods: " << nods.rows() << " " << nods.cols() << endl;

  // number of nods and quads
  const size_t N = nods.rows(), F = quad.rows();
  
  MatrixXi TRIS(4*F, 3);
  MatrixXd NODS(N+F, 3);
  NODS.topRows(N) = nods;
  for (size_t i = 0; i < F; ++i) {
    NODS.row(N+i) = nods(quad.row(i).array(), Eigen::all).colwise().sum()/4;

    int p0 = quad(i, 0), p1 = quad(i, 1), p2 = quad(i, 2), p3 = quad(i, 3);

    TRIS(4*i+0, 0) = p0;
    TRIS(4*i+0, 1) = p1;
    TRIS(4*i+0, 2) = N+i;

    TRIS(4*i+1, 0) = p1;
    TRIS(4*i+1, 1) = p2;
    TRIS(4*i+1, 2) = N+i;

    TRIS(4*i+2, 0) = p2;
    TRIS(4*i+2, 1) = p3;
    TRIS(4*i+2, 2) = N+i;

    TRIS(4*i+3, 0) = p3;
    TRIS(4*i+3, 1) = p0;
    TRIS(4*i+3, 2) = N+i;  
  }

  cout << "TRIS " << TRIS.rows() << " " << TRIS.cols() << endl;
  cout << "NODS " << NODS.rows() << " " << NODS.cols() << endl;
  igl::writeOBJ(argv[2], NODS, TRIS);

  cout << "[INFO] done" << endl;
  return 0;
}

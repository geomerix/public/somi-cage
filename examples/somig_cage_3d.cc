#include <igl/min_quad_with_fixed.h>
#include <igl/read_triangle_mesh.h>
#include <igl/opengl/glfw/Viewer.h>
#include <igl/project.h>
#include <igl/unproject.h>
#include <igl/snap_points.h>
#include <igl/unproject_onto_mesh.h>
#include <igl/colon.h>
#include <igl/slice_into.h>
#include <igl/opengl/glfw/imgui/ImGuiPlugin.h>
#include <igl/opengl/glfw/imgui/ImGuiMenu.h>
#include <igl/opengl/glfw/imgui/ImGuizmoWidget.h>
#include <Eigen/Core>
#include <iostream>
#include <stack>
#include <spdlog/spdlog.h>

#include "src/somigliana_3d.h"
#include "src/quadrule.hpp"

using namespace std;
using namespace Eigen;
using namespace green;

struct State
{
  // Rest and transformed control points
  Eigen::MatrixXd CV, CU;
  bool placing_handles = true;
} s;

namespace {
void ShowHelpMarker(const char* desc)
{
  ImGui::SameLine();
  ImGui::TextDisabled("(?)");
  if (ImGui::IsItemHovered()) {
    ImGui::BeginTooltip();
    ImGui::PushTextWrapPos(450.0f);
    ImGui::TextUnformatted(desc);
    ImGui::PopTextWrapPos();
    ImGui::EndTooltip();
  }
}
}

static void combined_mesh(const somig_deformer_3 &def,
                          MatrixXi &F, matd_t &V, matd_t &C) {
  // vertices  
  const size_t ncv = def.cageV_.cols(), nv = def.V_.cols();
  spdlog::info("ncv={}, nv={}", ncv, nv);
  V.resize(ncv+nv, 3);
  V << def.cageV_.transpose(), def.V_.transpose();
  
  // facets
  const size_t ncf = def.cageF_.cols(), nf = def.F_.cols();  
  F.resize(ncf+nf, 3);
  F << def.cageF_.transpose(), (def.F_.transpose().array()+ncv);

  // colors
  C.resize(F.rows(), 3);
  C << RowVector3d(0.2,0.3,0.8).replicate(ncf, 1),
      RowVector3d(1.0,0.7,0.2).replicate(nf, 1);
}

static string deformer_type(DeformerType s) {
  switch ( s ) {
    case SOMIGLIANA: return "somig";
    case GREEN:      return "green";
    case MEANVALUE:  return "mvc";
    case PHONG:      return "phong";
  }
  return "null";
}

int main(int argc, char *argv[])
{    
  if ( argc != 7 ) {
    cerr << "./igl_coo_deform mesh.obj cage.obj num_quadrature outdir [deformed_cage] [tet_of_cage]" << endl;
    return __LINE__;
  }

  const string outdir = argv[4];

  // Undo Management
  std::stack<State> undo_stack,redo_stack;
  const auto push_undo = [&](State & _s=s)
  {
    undo_stack.push(_s);
    // clear
    redo_stack = std::stack<State>();
  };
  const auto undo = [&]()
  {
    if(!undo_stack.empty())
    {
      redo_stack.push(s);
      s = undo_stack.top();
      undo_stack.pop();
    }
  };
  const auto redo = [&]()
  {
    if(!redo_stack.empty())
    {
      undo_stack.push(s);
      s = redo_stack.top();
      redo_stack.pop();
    }
  };

  long sel = -1;
  Eigen::RowVector3f last_mouse;

  // init deformer
  green::somig_deformer_3 deformer;
  deformer.load_mesh(argv[1]);
  deformer.load_cage(argv[2]);
  deformer.init(atoi(argv[3]));
  spdlog::info("quadrature number={}", 3*pow(atoi(argv[3]), 2));
  {
    char outf[256];
    sprintf(outf, "%s/rest_cage", outdir.c_str());
    deformer.save_cage(outf);
    sprintf(outf, "%s/rest_mesh", outdir.c_str());
    deformer.save_mesh(outf);    
  }
  
  // set meshes: cage + original mesh
  Eigen::MatrixXd V, U, D;
  Eigen::MatrixXi F;
  Eigen::MatrixXd color;    
  combined_mesh(deformer, F, V, color);
  U = V;
  D = Eigen::MatrixXd::Zero(V.rows(), V.cols());

  // precompute
  deformer.precompute_mvc_coords();
  deformer.precompute_green_coords();
  deformer.precompute_somig_coords();
  deformer.precompute_phong_coords(string(argv[6]));

  igl::opengl::glfw::Viewer viewer;
  igl::opengl::glfw::imgui::ImGuiPlugin plugin;
  viewer.plugins.push_back(&plugin);
  igl::opengl::glfw::imgui::ImGuiMenu menu;
  plugin.widgets.push_back(&menu);

  // Add a 3D gizmo plugin
  igl::opengl::glfw::imgui::ImGuizmoWidget gizmo;
  plugin.widgets.push_back(&gizmo);
  gizmo.T.block(0,3,3,1) =
    0.5*(V.colwise().maxCoeff() + V.colwise().minCoeff()).transpose().cast<float>();
  const Matrix4f T00 = gizmo.T;
  gizmo.visible = false;

  DeformerType typeDF   = SOMIGLIANA;
  BulgingType typeGamma = SOLID_ANGLE;
  float sc           = 0.1;
  bool plot_normal   = false;
  bool plot_deformed = false;
  float poissonR     = 0.0;
  float sqrt_gamma    = 0.0;
  float blend        = 0.0;
  menu.callback_draw_custom_window = [&]() {
    ImGui::SetNextWindowPos(ImVec2(180.f * menu.menu_scaling(), 10), ImGuiCond_FirstUseEver);
    ImGui::SetNextWindowSize(ImVec2(300, 300), ImGuiCond_FirstUseEver);
    ImGui::Begin("3D Cage deformer", nullptr, ImGuiWindowFlags_NoSavedSettings);
    ImGui::InputFloat("PoissonRatio", &poissonR, 0.00f, 1.0f, "%.3f");
    ImGui::SliderFloat("sqrt_gamma", &sqrt_gamma, 0.0f, 10.0f);
    ImGui::SliderFloat("blend", &blend, 0.0f, 1.0f); 

    ImGui::Combo("deformer", reinterpret_cast<int*>(&typeDF),
                 "Somigliana\0Green\0Meanvalue\0Phong\0\0");
    ImGui::Combo("bulging option", reinterpret_cast<int*>(&typeGamma),
                 "SolidAngle\0SweptVolume\0\0");
            
    ImGui::Checkbox("deformed mesh", &plot_deformed);
    ImGui::End();
  };

  // predefined colors
  const Eigen::RowVector3d orange(1.0,0.7,0.2);
  const Eigen::RowVector3d yellow(1.0,0.9,0.2);
  const Eigen::RowVector3d blue(0.2,0.3,0.8);
  const Eigen::RowVector3d green(0.2,0.6,0.3);
  const Eigen::RowVector3d red(0.8,0.2,0.2);
  const Eigen::RowVector3d white(1.0, 1.0, 1.0);
  const Eigen::RowVector3d cyan(0.0, 0.72, 0.92);
    
  const auto & update = [&]()
  {
    if(s.placing_handles) {
      viewer.data().show_texture = false;      
      viewer.data().set_vertices(V);
      viewer.data().set_data(vecd_t::Zero(V.rows()), igl::COLOR_MAP_TYPE_PARULA); 
      viewer.data().set_points(s.CV, orange);
      viewer.data().clear_edges();
    } else {
      if ( std::fabs(deformer.get_nu()-poissonR) > 1e-2 ) {
        // poisson ratio has been changed
        deformer.set_nu(poissonR);
        deformer.precompute_somig_coords();
      }
          
      Eigen::VectorXi b;
      igl::snap_points(s.CV, V, b);
      
      D.setZero();
      D(b.array(), Eigen::all) = s.CU-s.CV;
      U = V+D;

      matd_t bc, trac;
      deformer.deform(U, typeDF, typeGamma, sqrt_gamma, blend);

      if ( !plot_deformed ) {
        viewer.data().show_lines = true;
        viewer.data().show_faces = false;
        viewer.data().set_vertices(U);
        viewer.data().set_points(s.CU, blue);
        viewer.data().set_data(vecd_t::Zero(V.rows()), igl::COLOR_MAP_TYPE_PARULA);
      } else {
        viewer.data().show_lines = false;
        viewer.data().show_faces = true;
        viewer.data().show_texture = false;
        U.topRows(deformer.num_cage_vertices()).setZero();
        viewer.data().set_vertices(U);
        viewer.data().set_colors(cyan);
      }

      viewer.data().clear_edges();
      if ( plot_normal ) {
        if ( bc.size() != 0 && trac.size() != 0 ) {
          int nf = deformer.num_cage_facets(), ncv = deformer.num_cage_vertices();
          viewer.data().add_edges(bc.topRows(nf), (bc+sc*trac).topRows(nf), red);
          viewer.data().add_edges(bc.bottomRows(ncv), (bc+sc*trac).bottomRows(ncv), blue);
        }
      }
    }
  };

  // Attach callback to apply imguizmo's transform to mesh
  Eigen::Matrix4f T0 = gizmo.T;
  gizmo.callback = [&](const Eigen::Matrix4f &T)
  {
    const Eigen::Matrix4d TT = (T*T0.inverse()).cast<double>().transpose();
    if ( !s.placing_handles ) {
      U = (V.rowwise().homogeneous()*TT).rowwise().hnormalized();
      s.CU = (s.CV.rowwise().homogeneous()*TT).rowwise().hnormalized();
      Eigen::VectorXi b;
      igl::snap_points(s.CU, U, b);
    }    
    update();
  };
  
  viewer.callback_mouse_down = 
    [&](igl::opengl::glfw::Viewer&, int, int)->bool
  {
    last_mouse = Eigen::RowVector3f(
      viewer.current_mouse_x,viewer.core().viewport(3)-viewer.current_mouse_y,0);
    if(s.placing_handles)
    {
      // Find closest point on mesh to mouse position
      int fid;
      Eigen::Vector3f bary;
      if(igl::unproject_onto_mesh(
        last_mouse.head(2),
        viewer.core().view,
        viewer.core().proj, 
        viewer.core().viewport, 
        V, F, 
        fid, bary))
      {
        push_undo();

        if ( string(argv[1]).find("Cube") != std::string::npos ) {
          // this is for cube example          
          s.CV = V.row(12);
        } else {
          s.CV = V.topRows(deformer.num_cage_vertices());
        }
        
        gizmo.T.block(0,3,3,1) =
            0.5*(s.CV.colwise().maxCoeff() + s.CV.colwise().minCoeff()).transpose().cast<float>();
        T0 = gizmo.T;
        update();
        return true;
      }
    } else {
      // Move closest control point
      Eigen::MatrixXf CP;
      igl::project(
        Eigen::MatrixXf(s.CU.cast<float>()),
        viewer.core().view,
        viewer.core().proj, viewer.core().viewport, CP);
      Eigen::VectorXf D = (CP.rowwise()-last_mouse).rowwise().norm();
      sel = (D.minCoeff(&sel) < 30)?sel:-1;
      if(sel != -1)
      {
        last_mouse(2) = CP(sel,2);
        push_undo();
        update();
        return true;
      }
    }
    return false;
  };

  viewer.callback_mouse_up = [&](igl::opengl::glfw::Viewer&, int, int)->bool
  {
    sel = -1;
    return false;
  };
 
  viewer.callback_mouse_move = [&](igl::opengl::glfw::Viewer &, int,int)->bool
  {
    if(sel!=-1)
    {
      Eigen::RowVector3f drag_mouse(
        viewer.current_mouse_x,
        viewer.core().viewport(3) - viewer.current_mouse_y,
        last_mouse(2));
      Eigen::RowVector3f drag_scene,last_scene;
      igl::unproject(
        drag_mouse,
        viewer.core().view,
        viewer.core().proj,
        viewer.core().viewport,
        drag_scene);
      igl::unproject(
        last_mouse,
        viewer.core().view,
        viewer.core().proj,
        viewer.core().viewport,
        last_scene);
      s.CU.row(sel) += (drag_scene-last_scene).cast<double>();
      last_mouse = drag_mouse;
      update();
      return true;
    }
    return false;
  };

  static int count_save_times = 0;
  static int apply_s = 0;

  viewer.callback_key_pressed = 
    [&](igl::opengl::glfw::Viewer &, unsigned int key, int mod)
  {
    switch(key)
    { 
      case 'R':
      case 'r':
      {
        push_undo();
        s.CU = s.CV;
        break;
      }
      case 'Q':
      case 'q':
        {
          s.placing_handles = true;
          s.CV.resize(0, 0);
          s.CU.resize(0, 0);
          gizmo.T = T00;
          break;
        }
      case ' ':
        push_undo();
        s.placing_handles ^= 1;
        if(!s.placing_handles && s.CV.rows()>0)
        {
          // Switching to deformation mode
          s.CU = s.CV;
          Eigen::VectorXi b;
          igl::snap_points(s.CV,V,b);
        }
        break;
      case 'g':
      case 'G':
        push_undo();
        static double total_angle = 0.0;
        if ( !s.placing_handles ) {
          static const Vector3d rot_n = Vector3d::UnitZ();
          const double angle = 10.0/180*M_PI;
          mat3d R = AngleAxisd(angle, rot_n).toRotationMatrix();
          total_angle += angle;

          U = U*R.transpose();
          s.CU = s.CU*R.transpose();

          Eigen::VectorXi b;
          igl::snap_points(s.CU, U, b);
        }
        break;        
      case 's':
      case 'S':
        {
          char outf[256];

          sprintf(outf, "%s/cage-%03d-%s-nu-%f-eps-%f-w-%f", outdir.c_str(), count_save_times, deformer_type(typeDF).c_str(), poissonR, sqrt_gamma*sqrt_gamma, blend);
          deformer.save_cage(outf);

          sprintf(outf, "%s/mesh-%03d-%s-nu-%f-eps-%f-w-%f", outdir.c_str(), count_save_times, deformer_type(typeDF).c_str(), poissonR, sqrt_gamma*sqrt_gamma, blend);
          deformer.save_mesh(outf);

          ++count_save_times;
          spdlog::info("save cage and mesh {} times", count_save_times);
        }
        break;
      case 'x':
      case 'X':
        {
          if ( !s.placing_handles ) {
            MatrixXi defF; MatrixXd defV;
            igl::read_triangle_mesh(argv[5], defV, defF);
            if ( s.CU.rows() == defV.rows() ) {
              s.CU = defV;
            }
          }
        }
        break;
      case '1':
        {
          gizmo.visible = !gizmo.visible;
        }
        break;
      case '2':
        {
          gizmo.operation = ImGuizmo::TRANSLATE;
        }
        break;
      case '3':
        {
          gizmo.operation = ImGuizmo::ROTATE;
        }
        break;
      case '4':
        {
          gizmo.operation = ImGuizmo::SCALE;
        }
        break;
    }

    update();
    return true;
  };

  viewer.data().set_mesh(V,F); 
  viewer.data().face_based = true;
  viewer.data().show_lines = true;
  viewer.data().show_texture = false;
  viewer.core().is_animating = false;
  viewer.core().background_color.setOnes();
  update();
  return viewer.launch();
}

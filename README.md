# Somigliana Coordinates

This repository provides a reference implementation of our SIGGRAPH'23 conference paper "[Somigliana Coordinates: An Elasticity-derived Approach for Cage Deformation](https://jiongchen.github.io/files/somi-paper.pdf)". 

## Setup environment

The code was compiled on Linux 20.04 with CUDA 12.1 installed. Other required packages include:
```
apt install libboost-all-dev build-essential libopenblas-dev
```

## Compile
```
mkdir build 
cd build 
cmake -DCMAKE_BUILD_TYPE=Release -DLIBIGL_GLFW=ON -DLIBIGL_IMGUI=ON -DLIBIGL_OPENGL=ON .. 
make -j8
```

## Run 
Run the 2D cage deformer 
```
cd makefiles; ./run_cage_deformer2.sh Model_Index:Integer
```
and the 3D cage deformer
```
cd makefiles; ./run_cage_deformer3.sh Model_Index:Integer
```
Check out the model index and its corresponding model in the above two bash files.

## GUI instructions


| Commands             | Description |
| :---                 |    :----:   | 
| <kbd> Left click </kbd>    | Select all cage vertices       | 
| <kbd> Space     </kbd>     | Activate/Deactivate deformation mode        | 
| <kbd> Left drag </kbd>     | Move a cage vertex |
| <kbd> S,s       </kbd>     |    Save the current poses of the cage and mesh |
| <kbd> X,x       </kbd>     |         Load the provided cage pose |
| <kbd> 1         </kbd>     | Activate/Deactivate ```Gizmo``` transformation tool (3D only) |
| <kbd> 2,3,4     </kbd>     | Translate, rotate and scale the cage respectively (with ```Gizmo``` activated) |
| <kbd> Q,q       </kbd>     |         Reset      |   




## Contact
Author: [Jiong Chen](https://jiongchen.github.io/) <br/>
Email: jiong.chen@inria.fr <br/>
Please shoot me an email if you find any bugs, thanks!

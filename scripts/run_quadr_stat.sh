#!/bin/bash

num_threads=`echo "$(cat /proc/cpuinfo | grep processor | wc -l)/2" | bc`
echo "threads=${num_threads}"
export OMP_NUM_THREADS=${num_threads}

id=${1}
repo=("ManHead" "SpikyBox" "FireHydrant" "WireSphere" "Cactus" "Beast" "Ogre" "Bench" "Sphere" "Cube" "ManHeadL" "WireSphereB" "SymFireHydrant")
#        0          1            2            3          4        5      6       7        8      9     10           11               12

model=${repo[$id]}
cage=../data/3D/${model}_Cage_Tri.off
mesh=../data/3D/${model}_Tri.off
def_cage=../data/3D/${model}_Cage_Deformed_Tri.off
cage_tet=../data/3D/${model}_Cage_Tet.vtk

model_name="$(basename ${mesh} .off)"
outdir=../result/somig-cage/$(date -I)/${model_name}

echo "${model_name}"
mkdir -p ${outdir}
for num_quadrature in 10 20 30 40 50 64; do
    echo "${num_quadrature}"
    ../build/examples/somig_cage_3d ${mesh} ${cage} ${num_quadrature} ${outdir} ${def_cage} ${cage_tet} | tee ${outdir}/log-${num_quadrature}.txt
done

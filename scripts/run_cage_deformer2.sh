#!/bin/bash

num_threads=`echo "$(cat /proc/cpuinfo | grep processor | wc -l)/2" | bc`
echo "threads=${num_threads}"
export OMP_NUM_THREADS=${num_threads}

id=${1}
repo=("shapeL" "square-2048" "polygon" "sixedge" "star" "red_dragon" "blue_monster" "horse" "fatstar" "thinstar" "beam")
#        0           1           2          3      4         5            6            7       8         9         10

model=${repo[$id]}
cage=../data/2D/${model}_cage.off
mesh=../data/2D/${model}_mesh.off
def_cage=../data/2D/${model}_cage_deformed.off

model_name="$(basename ${mesh} .off)"
outdir=../result/somig-cage/$(date -I)/${model_name}
num_quadrature=128

echo "${model_name}"
mkdir -p ${outdir}
../build/examples/somig_cage_2d ${mesh} ${cage} ${num_quadrature} ${outdir} ${def_cage}
